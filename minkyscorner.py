
import sqlite3
import datetime
from flask import Flask, g, render_template, session, abort, request, flash, url_for, redirect
from contextlib import closing

app = Flask(__name__)
app.config.from_pyfile('config.py')

def get_now():
    return datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

@app.before_request
def log_request():
    if request.path not in (url_for(method) for method in ['dashboard', 'login', 'add_post']):
        h = request.headers
        now = get_now()
        user = h['User-Agent'] if 'User-Agent' in h else None
        ip = request.remote_addr
        get_db().execute('insert into visits (datetime, user, ip) values (?,?,?)',
                [now, user, ip])
        get_db().commit()

@app.route('/dashboard', methods=['GET'])
def dashboard():
    if not session.get('logged_in'):
        return redirect(url_for('login'))
    return render_template('dashboard.html')

@app.route('/post', methods=['POST'])
def add_post():
    if not session.get('logged_in'):
        abort(401)
    now = get_now()
    get_db().execute('insert into posts (datetime, title, body) values (?, ?, ?)',
            [now, request.form['title'], request.form['body']])
    get_db().commit()
    flash('new post successfully posted!')
    return redirect(url_for('musings'))

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/musings')
def musings():
    rows = query_db('select title, body from posts order by id desc')
    posts = [dict(title=row['title'], body=row['body']) for row in rows]
    return render_template('musings.html', posts=posts)

@app.route('/minecraft')
def minecraft():
    return render_template('minecraft.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/games')
def games():
    return render_template('games.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username != app.config['USER'] or password != app.config['PASSWORD']:
            error = 'Invalid username or password'
        else:
            session['logged_in'] = True
            flash('login successful!')
            return redirect(url_for('dashboard'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('logout successful')
    return redirect(url_for('index'))

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def init_db():
    with closing(connect_to_db()) as db:
        with app.open_resource(app.config['SCHEMA'], mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_to_db()
        db.row_factory = sqlite3.Row
    return db

def connect_to_db():
    return sqlite3.connect(app.config['DATABASE'])

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

if __name__ == '__main__':
    app.run(host='0.0.0.0')
