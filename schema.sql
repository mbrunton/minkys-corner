create table posts (
    id integer primary key autoincrement,
    datetime datetime not null,
    title text not null,
    body text not null
);
create table visits (
    id integer primary key autoincrement,
    datetime datetime not null,
    user text,
    ip text
);
