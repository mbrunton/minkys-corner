
import os
import minkyscorner
import unittest
import tempfile
import flask

class MinkysCornerTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, minkyscorner.app.config['DATABASE'] = tempfile.mkstemp()
        minkyscorner.app.config['TESTING'] = True
        minkyscorner.app.config['USER'] = 'admin'
        minkyscorner.app.config['PASSWORD'] = 'password'
        self.app = minkyscorner.app.test_client()
        minkyscorner.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(minkyscorner.app.config['DATABASE'])

    def login(self, username, password):
        data = dict(username=username, password=password)
        return self.app.post('/login', data=data, follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_login(self):
        r = self.login('admin', 'password')
        assert 'login successful' in r.data
        # TODO: check session has logged_in set to True on successful login
        r = self.logout()
        assert 'logout successful' in r.data
        r = self.login('wrongadmin', 'password')
        assert 'Invalid' in r.data
        r = self.login('admin', 'wrongpassword')
        assert 'Invalid' in r.data

    def test_post(self):
        data = dict(title='this is title', body='this is body')
        r = self.app.post('/post', data=data)
        assert r.status_code == 401
        self.login('admin', 'password')
        r = self.app.post('/post', data=data, follow_redirects=True)
        assert r.status_code == 200
        r = self.app.get('/musings')
        assert 'this is title' in r.data
        assert 'this is body' in r.data

    def test_about(self):
        r = self.app.get('/about')
        assert r.status_code == 200

    def test_games(self):
        r = self.app.get('/games')
        assert r.status_code == 200

    def test_minecraft(self):
        r = self.app.get('/minecraft')
        assert r.status_code == 200

    def test_dashboard(self):
        r = self.app.get('/dashboard')
        assert r.status_code == 302
        r = self.app.get('/dashboard', follow_redirects=True)
        assert r.status_code == 200
        self.login('admin', 'password')
        r = self.app.get('/dashboard')
        assert r.status_code == 200

    def test_views(self):
        pass

if __name__ == '__main__':
    unittest.main()
