#!/bin/bash

set -e

cd $(dirname "$0")
if [ "$#" -gt 0 ]; then
    ARG=$(echo "$1" | tr '[:upper:]' '[:lower:]')
    if [ "$ARG" == "prod" ]; then
        nohup ../venv/bin/python minkyscorner.py >> run.log &
    else
        echo "unrecognized argument: $1. aborting..."
        exit 1
    fi
else
    echo "DEBUG MODE ON!"
    echo "supply argument \"prod\" to run without debugging"
    ../venv/bin/python minkyscorner.py
fi
