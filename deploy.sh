#!/bin/bash

set -e

RUN=true

echo "deploying webserver..."
cd $(dirname "$0")/..
if [ ! -d webserver ]; then
  echo "expecting webserver folder in $(pwd). aborting"
  exit 1
fi
tar -czf webserver.tgz webserver
echo "sending to remote"
scp webserver.tgz main.asia-east1-a.minkys-corner:~
echo "starting server"
ssh main.asia-east1-a.minkys-corner "rm -r webserver && tar -xzf webserver.tgz"
if [ RUN ]; then
  ssh main.asia-east1-a.minkys-corner "cd webserver && ./run.sh prod"
fi
echo "SUCCESS!"
